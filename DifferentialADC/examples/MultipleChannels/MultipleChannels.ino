#include <DifferentialADC.h>
#include <Queu.h>

/*
	16-29			ADC+		ADC-		Gain	OccupiedChannel-Bits (Counting from 0)
	16				0			1			1x				1

	18				2			1			1x				2
	19				3			1			1x				3
	20				4			1			1x				4
	21				5			1			1x				5
	22				6			1			1x				6
	23				7			1			1x				7
DOESN'T WORK FOR MULTIPLE CHANNELS ABOVE 48

	48-61			ADC+		ADC-		Gain
	48				8			9			1x				8

	50				10			9			1x				9
	51				11			9			1x				10
	52				12			9			1x				11
	53				13			9			1x				12
	54				14			9			1x				13
	55				15			9			1x				14
	
*/

#define SampleTime 200

ADCManager ADManager;
DifferentialADC ADC1,ADC2,ADC3,ADC4,ADC5,ADC6,ADC7,ADC8;
long LastTime=0;
Queu * Q;

void setup()
{
  ADManager.Initialize();
  ADC1.Attach(16,&ADManager);
  byte Status=ADC2.Attach(52,&ADManager);
  Q=&ADManager.PendingConversions;
  Serial.begin(57600);
  Serial.print("\nFirstElement: ");
  Serial.print(Q->FirstElement(),DEC);
  Serial.print("\nLastElement: ");
  Serial.print(Q->LastElement(),DEC);  
  while(Serial.available()<=0)
  {
  }  
}

void loop()
{
  ADManager.Runner();
  long CurrentTime=millis();
  if(CurrentTime-LastTime>SampleTime)
  {
    LastTime=millis();
    if(Q->MessagesInQueu())
    {
      Serial.print("\nOption: ");
      Serial.print(Q->FirstElement(),DEC);    
    }
//    Serial.print("\nConversion ADC1: ");
//    Serial.print(ADC1.ReadConversion(),DEC);    
//    Serial.print("\nConversion ADC2: ");
//    Serial.print(ADC2.ReadConversion(),DEC);
    Serial.print("\n");
    Serial.print(ADManager.Conversion,DEC);
  }
}


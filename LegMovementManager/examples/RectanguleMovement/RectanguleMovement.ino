#include <LegMovementManager.h>
#include <BaBauHound.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>
#include <DifferentialADC.h>
#include <LabViewProtocol.h>

LegMovementManager MovementManager;
Leg LegA,LegB;
ADCManager ADManager;
DifferentialADC ADC1,ADC2;

float MovementParameters[4];
float ActualTime=0;
float LastTime=0;
int Elbow=ElbowDown;

Queu SQueu;

#define HipP 6
#define KneeP 10
#define Velocity 0.03

#define MovementRate 500
#define SampleRate 10

void setup()
{
  MovementManager.Initialize(); 
  
  ADManager.Initialize();
  ADC1.Attach(&ADManager);
  ADC2.Attach(&ADManager);
  
  MovementManager.SubscribeLeg(&LegA);
  
  Serial.begin(57600);
  SerialInitialize(&SQueu);

  LegA.Create(HipP,KneeP,0,0);
  LegA.InvertHip=False;
  LegA.InvertKnee=False; 
  LegA.Homming(ElbowDown);
  
  LegB.Create(HipP+1,KneeP+1,0,0);
  LegB.InvertHip=True;
  LegB.InvertKnee=False; 
  LegB.Homming(ElbowDown);  
 
  
  MovementParameters[0]=-60; //FinalX
  MovementParameters[1]=140; //FinalY
  MovementParameters[2]=Velocity; //Velocity
  MovementParameters[3]=Elbow; //Elbow
  
  MovementManager.MovementRequest(1,MovementParameters);

  MovementParameters[0]=-60; //FinalX
  MovementParameters[1]=120; //FinalY
  MovementParameters[2]=Velocity; //Velocity
  MovementParameters[3]=Elbow; //Elbow
  
  MovementManager.MovementRequest(1,MovementParameters);  
 
  MovementParameters[0]=60; //FinalX
  MovementParameters[1]=120; //FinalY
  MovementParameters[2]=Velocity; //Velocity
  MovementParameters[3]=Elbow; //Elbow
  
  MovementManager.MovementRequest(1,MovementParameters);

  MovementParameters[0]=60; //FinalX
  MovementParameters[1]=140; //FinalY
  MovementParameters[2]=Velocity; //Velocity
  MovementParameters[3]=Elbow; //Elbow
  
  MovementManager.MovementRequest(1,MovementParameters);
  
  MovementParameters[0]=0; //FinalX
  MovementParameters[1]=140; //FinalY
  MovementParameters[2]=Velocity; //Velocity
  MovementParameters[3]=Elbow; //Elbow
  
  MovementManager.MovementRequest(1,MovementParameters);  

  delay(3000);  
}

void loop()
{
    MovementManager.Runner();
    ADManager.Runner();
    
    ActualTime=millis();
    if(ActualTime-LastTime>=SampleRate)
    {
          LastTime=millis();
          SerialSubscribe(ADC1.ReadConversion()/2,1);
          SerialSubscribe(LegA.ReadHip(),2);
          SerialSubscribe(ADC2.ReadConversion()/2,3);          
          SerialSubscribe(LegA.ReadKnee(),4);          

          SerialSample();     
    }    
}
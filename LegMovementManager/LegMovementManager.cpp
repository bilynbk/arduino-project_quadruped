#include "LegMovementManager.h"

void LegMovementManager::Initialize()
{
	SubscribedLegs=0;
	MovementFinalX=0;
	MovementFinalY=0;
	MovementVelocity=0;
	MovementElbow=0;
	
	for(int i=0;i<MaxNumberOfLegs;i++)
	{
		LegBuffer[i]=0; //to null
		Movements[i].Initialize(); //no movements in queu
		Moving[i]=0; //not moving
	}
}

void LegMovementManager::SubscribeLeg(Leg * LegRequest)
{
	if(SubscribedLegs<4)
	{
		SubscribedLegs=SubscribedLegs+1;
		LegRequest->ID=SubscribedLegs;
		LegBuffer[SubscribedLegs-1]=LegRequest;
	}
}

//Check this method
void LegMovementManager::UnSubscribeLeg(int ID)
{
	if(SubscribedLegs>ID)
	{
		for(int i=ID;i<SubscribedLegs+1;i++)
		{
			if(i<SubscribedLegs)
				LegBuffer[i-1]=LegBuffer[i];
			else
				LegBuffer[i-1]=0; //to null
		}
		SubscribedLegs=SubscribedLegs-1;
	}
	else
	{
		LegBuffer[0]=0; //to null
		SubscribedLegs=0; //empty
	}
}

//MovementParameters[FinalX,FinalY,Velocity,Elbow] : ID=> LegID
void LegMovementManager::MovementRequest(int ID, float MovementParameters[4])
{
	float FinalX,FinalY,Velocity,Time,Travel;
	int Elbow;
	
	FinalX=MovementParameters[0];
	FinalY=MovementParameters[1];
	Velocity=MovementParameters[2];
	Elbow=MovementParameters[3];
	
	this->EnqueuMovement(FinalX,FinalY,Velocity,Elbow,ID);
}

void LegMovementManager::MovementHandler(int ID)
{
	if(this->isLegSubscribed(ID))
	{
		if(LegBuffer[ID-1]->FinishedMovement()||!LegBuffer[ID-1]->Traject.Running)
		{
 			if(Movements[ID-1].MessagesInQueu())
			{ 	
				Moving[ID-1]=1;//Set to moving
				// Serial.print("\nDeque ID: ");
				// Serial.print(ID,DEC);
				this->DequeuAndRefresh(ID);		
				LegBuffer[ID-1]->NewMovement(MovementFinalX,MovementFinalY,MovementVelocity,AccelerationFactor*MovementVelocity,MovementElbow);
 			}
			else
			{
				Moving[ID-1]=0; //Set to not moving
			}
		}
	}
}

//Enqueus 32 bits of long: 2 ID-bits : 1 Elbow-bit : 11 Velocity-bits : 9 PosY-bits : 9 PosX-bits
void LegMovementManager::EnqueuMovement(float FinalX, float FinalY, float Velocity, int Elbow, int ID) //up to 30 Movements (depending on Queu-Buffer Size)
{
	long CodedMovement=0;
	if(this->isLegSubscribed(ID))
	{
		long CodedFinalX,CodedFinalY;
		if(Elbow&1) //ElbowUp
		{
			CodedFinalX=this->EvaluateAndCodePosition(FinalX,EUMaxPosX,EUMinPosX);
			CodedFinalY=this->EvaluateAndCodePosition(FinalY,EUMaxPosY,EUMinPosY);
		}
		else //ElbowDown
		{
			CodedFinalX=this->EvaluateAndCodePosition(FinalX,EDMaxPosX,EDMinPosX);
			CodedFinalY=this->EvaluateAndCodePosition(FinalY,EDMaxPosY,EDMinPosY);
		}
		CodedFinalX=CodedFinalX&511;
		CodedFinalY=CodedFinalY&511;
		
		long CodedVelocity=1000*Velocity;
		if(Velocity>MaxVelocity)
			CodedVelocity=MaxVelocity*1000;
			
		long CodedElbow=Elbow;
		long CodedID=ID-1;
		CodedMovement=CodedFinalX|(CodedFinalY<<9)|(CodedVelocity<<18)|(CodedElbow<<29)|(CodedID<<30);
		
		Movements[ID-1].Enqueu(CodedMovement);
	}
}//Coded ID from 0 to 3

//Dequeus 32 bits of long: 2 ID-bits : 1 Elbow-bit : 11 Velocity-bits : 9 PosY-bits : 9 PosX-bits
int LegMovementManager::DequeuAndRefresh(int ID)
{
	long CodedMovement;
	if(Movements[ID-1].MessagesInQueu())
	{
		CodedMovement=Movements[ID-1].Dequeu();
		ID=long((CodedMovement>>30)&3)+1;
		MovementElbow=long((CodedMovement>>29)&1);
		MovementVelocity=long((CodedMovement>>18)&2047);
		MovementVelocity=MovementVelocity/1000;
		MovementFinalY=this->DecodePosition(long((CodedMovement>>9)&511));
		MovementFinalX=this->DecodePosition(long(CodedMovement&511));

	}
	else
		ID=0; //no leg
	
	return ID;
}

bool LegMovementManager::isLegSubscribed(int ID)
{
	if(ID>0&&ID<SubscribedLegs+1)
		return True;
	else
		return False;
}

void LegMovementManager::Runner()
{
	for(int i=0;i<SubscribedLegs;i++)
	{
		int ID=i+1;
		this->MovementHandler(ID);	
		LegBuffer[i]->Move();
	}
}

// 1 sign-bit : 8 Position-bits
long LegMovementManager::EvaluateAndCodePosition(float FinalX,int MaxPos,int MinPos)
{
	long CodedPosition=0; //(only 8 bits are usefull)
	//Detect sign
	if(FinalX<0) //negative
	{
		if(FinalX>MinPos)
		{			
			if(FinalX>MaxPos)//out of range above
			{					
				CodedPosition=fabs(MaxPos);		
			}
			else
			{					
				CodedPosition=fabs(FinalX);
			}
			//Transform into Two's Complement
			CodedPosition=~CodedPosition;
			CodedPosition=CodedPosition+1;			
			
			//Set 9th bit
			CodedPosition=CodedPosition|(1<<8);				
		}
		else //out of range below
		{
			CodedPosition=fabs(MinPos);	
			if(MinPos<0) //if negative
			{			
				//Transform into Two's Complement
				CodedPosition=~CodedPosition;
				CodedPosition=CodedPosition+1;
				
				//Set 9th bit
				CodedPosition=CodedPosition|(1<<8);				
			}
			else
			{
				//UnSet 9th bit
				CodedPosition=CodedPosition&(~(1<<8));			
			}
		}		
	}
	else //Possitive FinalX
	{
		if(FinalX<MaxPos)
		{
			if(FinalX<MinPos) //out of range below
				CodedPosition=MinPos;
			else
				CodedPosition=fabs(FinalX);
		}
		else //out of range above
		{
			CodedPosition=fabs(MaxPos);		
		}	
	}
	return CodedPosition;
}

float LegMovementManager::DecodePosition(long CodedPosition)
{
	float Position=0;
	long AuxiliarPosition=0;
	//is suposed to be within the range
	if(CodedPosition&(1<<8)) //negative value
	{		
		AuxiliarPosition=(~CodedPosition)&255;
		AuxiliarPosition=AuxiliarPosition+1;
		
		Position=float(AuxiliarPosition);
		Position=-Position;
	}
	else
	{
		AuxiliarPosition=CodedPosition&255;
		Position=float(AuxiliarPosition);			
	}
	return Position;
}

// void LegMovementManager::SerialMovementSubscribe(long CodedSample)
// {
// }

uint8_t LegMovementManager::SubscribedMovements()
{
	uint8_t NumberOfSubscribedMovements=0;
	for(int i=0;i<MaxNumberOfLegs;i++)
	{
		uint8_t NumberInQueu=0;
		NumberInQueu=Movements[i].MessagesInBuffer();
		NumberOfSubscribedMovements=NumberOfSubscribedMovements+NumberInQueu;
	}
	return NumberOfSubscribedMovements;
}

bool LegMovementManager::FinishedAllMovements()
{
	uint8_t NumberOfSubscribedMovements=0;
	NumberOfSubscribedMovements=this->SubscribedMovements();
	if(NumberOfSubscribedMovements==0)
	{
		uint8_t counter=0;
		for(int i=0;i<MaxNumberOfLegs;i++)
		{
			if(!Moving[i])
				counter++;
		}
		if(counter>=MaxNumberOfLegs)
			return 1; //FinishedAllMovements
	}
	return 0;
}
#include <OsqarSerialProtocol.h>

Sample TestSample;
Sample SampleArray[7];

void setup()
{
  TestSample.Initialize();
  for(int i=0;i<7;i++)
  { 
    SampleArray[i].Initialize();
  }
  Serial.begin(57600);
  while(Serial.available()<=0)
  {
  }
  
  TestSample.Add(5);
  TestSample.Add(19);
  TestSample.Add(-2); 
  TestSample.Add(-30);
  TestSample.Add(-19);
  TestSample.Add(15);   
  TestSample.Add(31);
  TestSample.Add(45);   
  TestSample.Add(100);
  TestSample.Add(54);
  Serial.print("\nHow Many Added Before Delete: ");
  Serial.print(TestSample.HowManyAdded());
  TestSample.ReleaseSpot(3);
  Serial.print("\nHow Many Added After Delete: ");
  Serial.print(TestSample.HowManyAdded());  
  TestSample.Add(30);  
  Serial.print("\nHow Many Added After Write: ");
  Serial.print(TestSample.HowManyAdded());  
  for(int i=0;i<SampleSize;i++)
  {
    Serial.print("\nData ");
    Serial.print(i+1);
    Serial.print(": ");
    Serial.print(TestSample.ReadData(i));
  }
}

void loop()
{
}


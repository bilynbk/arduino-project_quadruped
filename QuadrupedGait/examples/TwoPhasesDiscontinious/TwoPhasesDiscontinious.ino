#include <QuadrupedGait.h>
#include <StateMachine.h>
#include <BaBauHound.h>
#include <LegMovementManager.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>
#include <DifferentialADC.h>
#include <LabViewProtocol.h>

#define SampleRate 10

float ActualTime=0;
float LastTime=0;

Quadruped Osqar;
TwoPhasesDiscontinious TPD;
Queu QSample;

bool Pause;

ADCManager ADManager;
DifferentialADC ADC1,ADC2;

void setup()
{
  ADManager.Initialize();
  ADC1.Attach(&ADManager);
  ADC2.Attach(&ADManager);  
  
  Osqar.Initialize();
  TPD.Initialize(&Osqar);  
  
  Serial.begin(57600);
  SerialInitialize(&QSample);

  delay(5000);
}
void loop()
{
  TPD.Runner(Pause,3);
  
  if(Serial.available()>0)
  {
    TPD.Selector=Serial.parseInt();
    if(TPD.Selector==3)
    {
      Pause=1;
    }
    else
    {
      Pause=0;
      Serial.parseInt();
    }
  }
  
    ADManager.Runner();
    
    ActualTime=millis();
    if(ActualTime-LastTime>=SampleRate)
    {
          LastTime=millis();
          SerialSubscribe(Osqar.Legs[3].ReadHip(),1);
          SerialSubscribe(fabs(Osqar.Legs[1].ReadHip()),2);
          SerialSubscribe(Osqar.Legs[3].ReadKnee(),3);          
          SerialSubscribe(fabs(Osqar.Legs[1].ReadKnee()),4);          

          SerialSample();
    }   
}
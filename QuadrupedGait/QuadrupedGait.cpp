#include "QuadrupedGait.h"

//
//		CLASS GaitPlanner
//

void GaitPlanner::Runner(bool PauseRequest,float CommandedVelocity)
{
	GaitPause=PauseRequest;
	
	if(fabs(CommandedVelocity)<=MaxMovementVelocity)
		MovementVelocity=fabs(CommandedVelocity);
	else
		MovementVelocity=MaxMovementVelocity;	
	
	AngleVelocity=MovementVelocity;
	STM.Runner();
}

//Possible Functions for states
GaitPlanner * WalkingPlanner;

uint8_t Nothing()
{
	return WalkingPlanner->Selector;
}

uint8_t DoOut()
{
	WalkingPlanner->Selector=0;
	return 0;
}

uint8_t InitializeMovement()
{
	//Serial.print("\nEntrando al estado START");

	WalkingPlanner->LegManager.Initialize();
	//Subscribe legs to LegManager
/* 	for(int i=0;i<4;i++)
	{
		WalkingPlanner->LegManager.SubscribeLeg(&WalkingPlanner->Osqar->Legs[i]);
	} */
	WalkingPlanner->LegManager.SubscribeLeg(&WalkingPlanner->Osqar->Legs[0]);
	WalkingPlanner->LegManager.SubscribeLeg(&WalkingPlanner->Osqar->Legs[1]);
	WalkingPlanner->LegManager.SubscribeLeg(&WalkingPlanner->Osqar->Legs[2]);	
	WalkingPlanner->LegManager.SubscribeLeg(&WalkingPlanner->Osqar->Legs[3]);
	
	delay(1000);
	
	float MovementParameters[4];
	//Subscribe movements to legs
	//Leg 1: MovementParameters
	MovementParameters[0]=(MidDistance+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[0];
	MovementParameters[2]=WalkingPlanner->AngleVelocity;//WalkingPlanner->MovementVelocity*180/(PI*165);
	MovementParameters[3]=ElbowUp;
	
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);

 	//Leg 2: MovementParameters	
	MovementParameters[0]=(RearSW+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);	

	//Leg 3: MovementParameters	
	MovementParameters[0]=MidDistance+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[2];		
	MovementParameters[3]=ElbowDown;	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);
	
	//Leg 4: MovementParameters	
	MovementParameters[0]=RearSW+WalkingPlanner->XLegOffset[3];		
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[3];		
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);
	
	WalkingPlanner->MovingLeg=&WalkingPlanner->Osqar->Legs[0];
	
	return 0;
}

uint8_t RearRightLegMovement()
{
	WalkingPlanner->MovingLeg=&WalkingPlanner->Osqar->Legs[3];	
	//Serial.print("\nEntrando a estado RearLeg");
		//Serial.print("\nRight Side");
		
	float MovementParameters[4];
	//Subscribe movements to legs
	//Leg 1: MovementParameters
	MovementParameters[0]=(MidDistance+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[0];
	MovementParameters[2]=WalkingPlanner->AngleVelocity;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[3]=ElbowUp;
		
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);

	//Leg 2: MovementParameters	
	MovementParameters[0]=(RearSW+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);	

	//Leg 3: MovementParameters	
	MovementParameters[3]=ElbowDown;	
	MovementParameters[0]=MidDistance+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[2];	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);
		
	//Leg 4: MovementParameters	
	MovementParameters[0]=RearSW+WalkingPlanner->XLegOffset[3];
	MovementParameters[1]=Floor-Height+WalkingPlanner->YLegOffset[3];	
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);		
	
	MovementParameters[0]=FrontSW+WalkingPlanner->XLegOffset[3];
	MovementParameters[1]=Floor-Height+WalkingPlanner->YLegOffset[3];	
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);		
	
	MovementParameters[2]=WalkingPlanner->AngleVelocity/2;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[0]=FrontSW+WalkingPlanner->XLegOffset[3];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[3];	
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);		

	return 0;
}

uint8_t RearLeftLegMovement()
{
	WalkingPlanner->MovingLeg=&WalkingPlanner->Osqar->Legs[2];	
	//Serial.print("\nLeft Side");	
	float MovementParameters[4];
	//Subscribe movements to legs
	//Leg 1: MovementParameters
	MovementParameters[0]=(RearSW+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[0];
	MovementParameters[2]=WalkingPlanner->AngleVelocity;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[3]=ElbowUp;
	
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);

	//Leg 2: MovementParameters	
	MovementParameters[0]=(MidDistance+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);	

	//Leg 3: MovementParameters	
	MovementParameters[0]=RearSW+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor-Height+WalkingPlanner->YLegOffset[2];	
	MovementParameters[3]=ElbowDown;	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);		

	MovementParameters[0]=FrontSW+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor-Height+WalkingPlanner->YLegOffset[2];	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);		

	MovementParameters[2]=WalkingPlanner->AngleVelocity/2;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[0]=FrontSW+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[2];	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);
	
	MovementParameters[2]=WalkingPlanner->AngleVelocity;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/	
	//Leg 4: MovementParameters	
	MovementParameters[0]=MidDistance+WalkingPlanner->XLegOffset[3];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[3];	
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);	

	return 0;
}

uint8_t FrontRightLegMovement()
{
	WalkingPlanner->MovingLeg=&WalkingPlanner->Osqar->Legs[1];
	//Serial.print("\nEntrando a estado FrontLeg");

	//Serial.print("\nRight Side");	
	float MovementParameters[4];
	//Subscribe movements to legs
	//Leg 1: MovementParameters
	MovementParameters[0]=(MidDistance+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[0];
	MovementParameters[2]=WalkingPlanner->AngleVelocity;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[3]=ElbowUp;
		
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);

 	//Leg 2: MovementParameters	
	MovementParameters[0]=(RearSW+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor-Height+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);		

	MovementParameters[0]=(FrontSW+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor-Height+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);		

	MovementParameters[2]=WalkingPlanner->AngleVelocity/2;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[0]=(FrontSW+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);	

	MovementParameters[2]=WalkingPlanner->AngleVelocity;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	//Leg 3: MovementParameters	
	MovementParameters[3]=ElbowDown;	
	MovementParameters[0]=MidDistance+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[2];	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);
		
	//Leg 4: MovementParameters	
	MovementParameters[0]=FrontSW+WalkingPlanner->XLegOffset[3];	
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[3];
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);		
	
	return 0;
}

uint8_t FrontLeftLegMovement()
{
	WalkingPlanner->MovingLeg=&WalkingPlanner->Osqar->Legs[0];	
	//Serial.print("\nLeft Side");	
	float MovementParameters[4];
	//Subscribe movements to legs
	//Leg 1: MovementParameters
	MovementParameters[2]=WalkingPlanner->AngleVelocity;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[3]=ElbowUp;
		
	MovementParameters[0]=(RearSW+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor-Height+WalkingPlanner->YLegOffset[0];	
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);		

	MovementParameters[0]=(FrontSW+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor-Height+WalkingPlanner->YLegOffset[0];	
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);		

	MovementParameters[2]=WalkingPlanner->AngleVelocity/2;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[0]=(FrontSW+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[0];	
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);

	MovementParameters[2]=WalkingPlanner->AngleVelocity;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
 	//Leg 2: MovementParameters	
	MovementParameters[0]=(MidDistance+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);	

	//Leg 3: MovementParameters	
	MovementParameters[3]=ElbowDown;
	MovementParameters[0]=FrontSW+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[2];	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);
		
	//Leg 4: MovementParameters	
	MovementParameters[0]=MidDistance+WalkingPlanner->XLegOffset[3];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[3];	
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);	

	return 0;
}

uint8_t RightCOGMovement()
{
	WalkingPlanner->MovingLeg=&WalkingPlanner->Osqar->Legs[0];
	//Serial.print("\nEntrando a estado MoveCOG");
	int Counter=WalkingPlanner->LegCounter;
	//Serial.print("\nRight Side");	
	float MovementParameters[4];
	//Subscribe movements to legs
	//Leg 1: MovementParameters
	MovementParameters[0]=(RearSW+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[0];
	MovementParameters[2]=WalkingPlanner->AngleVelocity/2;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[3]=ElbowUp;
		
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);

	//Leg 2: MovementParameters	
	MovementParameters[0]=(MidDistance+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);	

	//Leg 3: MovementParameters	
	MovementParameters[3]=ElbowDown;
	MovementParameters[0]=RearSW+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[2];	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);
		
	//Leg 4: MovementParameters	
	MovementParameters[0]=MidDistance+WalkingPlanner->XLegOffset[3];		
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[3];	
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);		

	
	if(WalkingPlanner->LegCounter<1)
		WalkingPlanner->LegCounter=WalkingPlanner->LegCounter+1;
	else //end of second phase
		WalkingPlanner->LegCounter=0;	
		
	return 0;
}

uint8_t LeftCOGMovement()
{
	WalkingPlanner->MovingLeg=&WalkingPlanner->Osqar->Legs[0];
	//Serial.print("\nLeft Side");	
	float MovementParameters[4];
	//Subscribe movements to legs
	//Leg 1: MovementParameters
	MovementParameters[0]=(MidDistance+WalkingPlanner->XLegOffset[0]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[0];
	MovementParameters[2]=WalkingPlanner->AngleVelocity/2;/*WalkingPlanner->MovementVelocity*180/(PI*165);*/
	MovementParameters[3]=ElbowUp;
	
	WalkingPlanner->LegManager.MovementRequest(1,MovementParameters);

	//Leg 2: MovementParameters	
	MovementParameters[0]=(RearSW+WalkingPlanner->XLegOffset[1]);
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[1];	
	WalkingPlanner->LegManager.MovementRequest(2,MovementParameters);	

	//Leg 3: MovementParameters
	MovementParameters[3]=ElbowDown;	
	MovementParameters[0]=MidDistance+WalkingPlanner->XLegOffset[2];
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[2];	
	WalkingPlanner->LegManager.MovementRequest(3,MovementParameters);
		
	//Leg 4: MovementParameters	
	MovementParameters[0]=RearSW+WalkingPlanner->XLegOffset[3];		
	MovementParameters[1]=Floor+WalkingPlanner->YLegOffset[3];	
	WalkingPlanner->LegManager.MovementRequest(4,MovementParameters);	

	return 0;
}

uint8_t MoveLegs()
{	
	//uint8_t NumberOfSubscribedMovements=0;

	WalkingPlanner->LegManager.Runner();	
	
	//NumberOfSubscribedMovements=WalkingPlanner->LegManager.SubscribedMovements();
	//until all legs are in their position
	if(WalkingPlanner->LegManager.FinishedAllMovements())
	return WalkingPlanner->Selector;
	
	else
	return 0;
}

//
//		CLASS TwoPhasesDiscontinious
//

void TwoPhasesDiscontinious::Initialize(Quadruped * InitializedQuadruped)
{
	Selector=0;
	
	WalkingPlanner=this;
	LegCounter=0;
	Osqar=InitializedQuadruped;
	AngleVelocity=DefaultAngleVelocity;
	
	for(int i=0;i<4;i++)
	{
		this->SetOffsetXY(i,0,0);
	}
	
	// this->SetOffsetXY(0,-30,20);
	// this->SetOffsetXY(1,-30,20);
	// this->SetOffsetXY(2,0,10);
	
	Start.Create(&RearRightLeg,MoveLegs,InitializeMovement,DoOut);
	//AssureStaticStability.Create(&RearLeg,&FrontLeg,Nothing,Nothing,DoOut);
	RearRightLeg.Create(&FrontRightLeg,MoveLegs,RearRightLegMovement,DoOut);
	FrontRightLeg.Create(&MoveRightCOG,MoveLegs,FrontRightLegMovement,DoOut);
	
	RearLeftLeg.Create(&FrontLeftLeg,MoveLegs,RearLeftLegMovement,DoOut);
	FrontLeftLeg.Create(&MoveLeftCOG,MoveLegs,FrontLeftLegMovement,DoOut);	
	
	MoveRightCOG.Create(&RearLeftLeg,&Pause,MoveLegs,RightCOGMovement,DoOut);
	MoveLeftCOG.Create(&RearRightLeg,&Pause,MoveLegs,LeftCOGMovement,DoOut);	
	
	Pause.Create(&RearRightLeg,Nothing,Nothing,DoOut);
	
	STM.Initialize(&Start);
}

void GaitPlanner::SetOffsetXY(int LegIndex,int X, int Y)
{
	XLegOffset[LegIndex]=X;
	YLegOffset[LegIndex]=Y;
}


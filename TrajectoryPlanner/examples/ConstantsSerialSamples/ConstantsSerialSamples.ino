#include <TrajectoryPlanner.h>
#include <LabViewProtocol.h>
#include <Queu.h>

#define ElbowDown 0
#define ElbowUp 1

LabViewProtocol SerialTransmision;
Queu Test;

TrajectoryPlanner LegDTraject;

int i=3;
int j=0;
float LastTime=0;
float InitialTime=0;

void setup()
{
  Serial.begin(57600);
  LegDTraject.Initialize();
  SerialInitialize(&Test);
//void TrajectoryPlanner::NewTraject(float XInitial,float YInitial, float XFinal, float YFinal, float Time(mseg), int Elbow)  
  LegDTraject.NewTraject(-20,135,-114,14,10000,ElbowDown);
  InitialTime=millis();

////Sampled Hip and Knee  
  for(int i=0;i<NumberOfSamples;i++)
  {
  }
  
//HipSplineConstants
//  for(int i=0;i<4;i++)
//  {
//    switch(i)
//    {
//      case 0:
//        Serial.print("\nHipA=[");
//      break;
//      case 1:
//        Serial.print("\nHipB=[");      
//      break;
//      case 2:
//        Serial.print("\nHipC=[");      
//      break;
//      case 3:
//        Serial.print("\nHipD=[");      
//      break;
//      default:
//        Serial.print("%");
//    }
//    for(int j=0;j<NumberOfSamples;j++)
//    {     
//      Serial.print(LegDTraject.HipConstants[j+i*NumberOfSamples],DEC);
//      Serial.print(" ");
//    }
//    Serial.print("];");
//  }
//
//
////KneeSplineConstats  
//  for(int i=0;i<4;i++)
//  {
//    switch(i)
//    {
//      case 0:
//        Serial.print("\nKneeA=[");
//      break;
//      case 1:
//        Serial.print("\nKneeB=[");      
//      break;
//      case 2:
//        Serial.print("\nKneeC=[");      
//      break;
//      case 3:
//        Serial.print("\nKneeD=[");      
//      break;
//      default:
//        Serial.print("%");
//    }
//    for(int j=0;j<NumberOfSamples;j++)
//    {     
//      Serial.print(LegDTraject.KneeConstants[j+i*NumberOfSamples],DEC);
//      Serial.print(" ");
//    }
//    Serial.print("];");
//  } 
// 

  delay(5000); //Cause of labview
}

void loop()
{
  float ActualTime=millis();
  float Hip,Knee;
   
  if(ActualTime-LastTime>10/*500&&i<25*/)
  {
    LastTime=millis();
    LegDTraject.Runner(); 
    
    SerialSubscribe(LegDTraject.HipArray[j],5,LegDTraject.KneeArray[j]);
    SerialSubscribe(LegDTraject.Hip,2);
    SerialSubscribe(LegDTraject.Knee,4);     
      
    SerialSample();   
    i++;
    if(i%20==0)
      j++;
    if(j>NumberOfSamples)
    {
      j=0;
    }
  }
}
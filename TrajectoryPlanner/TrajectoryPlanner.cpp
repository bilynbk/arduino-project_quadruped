#include "TrajectoryPlanner.h"

/*----------------------------------------------------------------
------------------------------------------------------------------
					TrajectoryPlanner Class
------------------------------------------------------------------
------------------------------------------------------------------
*/
/*
**----------------------------------------------------------------
**	TrajectoryPlanner CLASS Method Initialize()
**	PRE:	Initialize Values
**
**	POST:
**
**----------------------------------------------------------------
*/
void TrajectoryPlanner::Initialize()
{	
	//Traject Parameters
	InitialPositionX=0;
	InitialPositionY=0;
	FinalPositionX=0;
	FinalPositionY=0;
	FinalTime=0;
	TrackTime=0;
	InitialTime=millis();
	
	//InverseKinematic Parameters
	InverseKinematicHip=0;
	InverseKinematicKnee=0;
	FinishedTrajectory=False;
	Running=False;

	for(int i=0;i<=NumberOfSamples;i++)
	{
		HipArray[i]=0;
		KneeArray[i]=0;
	}
}

/*
**----------------------------------------------------------------
**	TrajectoryPlanner CLASS Method NewTraject()
**	PRE:	
**
**	POST: Hip and Knee NumberOfSamples+1 sized array
**
**----------------------------------------------------------------
*/
void TrajectoryPlanner::NewTraject(float XInitial,float YInitial, float XFinal, float YFinal, float Time, int Elbow)
{
    this->Initialize();
			
	//Refresh movement parameters
	InitialPositionX=XInitial;
	InitialPositionY=YInitial;
	FinalPositionX=XFinal;
	FinalPositionY=YFinal;
	FinalTime=Time;
    TrackTime=Time/NumberOfSamples;
	TrackNumber=0;
	
	//Setting joint array
	this->SetJointArray(Elbow);
}

/*
**----------------------------------------------------------------
**	TrajectoryPlanner CLASS Method DeleteTraject()
**	PRE:	
**
**	POST:
**
**----------------------------------------------------------------
*/
void TrajectoryPlanner::DeleteTraject()
{
        this->Initialize();
}

/*
**-----------------------------------------------------------------
**	TrajectoryPlanner CLASS Method SetJointArray()
**	PRE:	InitialPosition,FinalPosition for XY
**	POST:	JointArray of NumberOfSamples+1 size for each XY position
			REMEMBER
			*Hip and Knee Array has N+1 points (N Sampled + Initial)
**-----------------------------------------------------------------
*/
void TrajectoryPlanner::SetJointArray(int Elbow) //This is the function that takes care of the trajectory
{
	float DeltaX=(FinalPositionX-InitialPositionX)/NumberOfSamples;
	float DeltaY=(FinalPositionY-InitialPositionY)/NumberOfSamples;
    
	// y=mx+b => m=(FinalPositionY-InitialPositionY)/(FinalPositionX-InitialPositionX)
	// y=mx+b	y=m*(i*DeltaX)+Xo
	float m,u;
	m=DeltaY/DeltaX;
	u=DeltaX/DeltaY;
	
	for(int i=0;i<=NumberOfSamples;i++)
	{
		float ActualPositionX;
		float ActualPositionY;
		
		if(isinf(m)||isinf(m))
		{
			ActualPositionY=InitialPositionY+i*DeltaY;
			ActualPositionX=(u*i*DeltaY)+InitialPositionX;		
		}
		else
		{
			ActualPositionX=InitialPositionX+i*DeltaX;
			ActualPositionY=(m*i*DeltaX)+InitialPositionY;
		}
		
		this->InverseKinematics(ActualPositionX,ActualPositionY,Elbow);	
		HipArray[i]=InverseKinematicHip;
		KneeArray[i]=InverseKinematicKnee;           
	}
	//Initialize Hip and Knee cubic spline coeficientes
	this->CubicSpline(HipArray,HipConstants);
	this->CubicSpline(KneeArray,KneeConstants);
}

/*
**-------------------------------------------------------------------
**		v2.0
**		TrajectoryPlanner CLASS Method InverseKinematics()
**		PRE:	XY position. Hip represents(0,0)
**		POST:
--------------------------------------------------------------------*/
bool TrajectoryPlanner::InverseKinematics(float XPosition, float YPosition,int Case)
{
	float LinkSumSq,LinkDiffSq,XPositionSq,YPositionSq,Phi,Psi;
	float HipAux;//Q1
	float KneeAux;//Q2
	/**/
	LinkSumSq=HipLink+KneeLink;
	LinkSumSq=sq(LinkSumSq);
	/**/
	LinkDiffSq=HipLink-KneeLink;
	LinkDiffSq=sq(LinkDiffSq);
	/**/
	XPositionSq=XPosition;
	XPositionSq=sq(XPositionSq);
	/**/
	YPositionSq=YPosition;
	YPositionSq=sq(YPositionSq);
	
	/****************************/
	/*If Valid (Jacobian)!=0*/

	/****************************/
	//		Q2 = Knee
	KneeAux=(LinkSumSq-XPositionSq-YPositionSq)/(XPositionSq+YPositionSq-LinkDiffSq);
	KneeAux=sqrt(KneeAux);
	switch (Case)
	{
		case 0://Elbow Down
			KneeAux=2*atan(KneeAux);//Radians
			break;
		case 1://Elbow Up
			KneeAux=-2*atan(KneeAux);//Radians
			break;
		default:
			return -1;
	}
	/****************************/
	//		Q1 = Hip
	Phi=atan2(YPosition,XPosition);
	Psi=atan2(KneeLink*sin(KneeAux),HipLink+KneeLink*cos(KneeAux));
	
	HipAux=Phi-Psi;//Radians
	HipAux=180*HipAux/PI;
	KneeAux=180*KneeAux/PI;//Degrees	
	
	InverseKinematicHip=HipAux;
	InverseKinematicKnee=KneeAux;
	return 1;
}

/*--------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Leg::CubicSplin		Splin Cubic Method
PRE:	Needs final and initial Q position, initial and final Acceleration and Velocity
POST:

Example:
X[NumberOfSamples+1] -> Initialize
Constant[4*(NumberOfSamples-1)] -> Doesn't need to be initialized
GenerateCubicSpline(X,&Constant[0]);
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
*/
void TrajectoryPlanner::CubicSpline(float * Y,float *Constant) //Hip or Knee Array; Hip or Knee Constants Array //initializer
{  
  //CubicInitialized=True;
 
  // %CONSTANTS for array
  int InitialIndex=0;
  //int N=SizeOfSample;
  int N=NumberOfSamples+1;
  // %SplinCubico forcing zero both ends  
  // %If we have a defined number of paired X,Y points (These would be ti,Qi)
  //float X[]={0,1,2,4};
  float X[N];
  for(int i=0;i<N;i++)
  {
  	X[i]=i*TrackTime/CubicTimeFactor;//*/sqrt(X[i]);
  }
  
  /***********************************************************************************
  				Need sampling couple: Q[N],T[N];
  ************************************************************************************/  
  // %Setting general matrix NxN  
  float PrimaryMatrix[N*N];
  
  //Setting PrimaryMatrix
  for(int i=0;i<N*N;i++)
  {
    PrimaryMatrix[i]=0;
  }
  for(int i=InitialIndex;i<=N-1+InitialIndex;i++)//ROW
  {
      int DiagonalRow=i-InitialIndex;
      int DiagonalColumn=i;
      int DiagonalPosition=N*DiagonalRow+DiagonalColumn;
      if(i==InitialIndex)//%First Row
      {
          PrimaryMatrix[i]=2*(X[i+1]-X[i]);
          PrimaryMatrix[i+1]=X[i+1]-X[i];
      }
      else
      {
        if (i==N-1+InitialIndex)//%LastRow
        {
            PrimaryMatrix[DiagonalPosition-1]=X[i]-X[i-1];
            PrimaryMatrix[DiagonalPosition]=2*(X[i]-X[i-1]);
        }      
        else
        {
          PrimaryMatrix[DiagonalPosition-1]=X[i]-X[i-1];
          PrimaryMatrix[DiagonalPosition]=2*((X[i]-X[i-1]+X[i+1]-X[i]));
          PrimaryMatrix[DiagonalPosition+1]=X[i+1]-X[i];
        }
      }     
  }
  // %We define the primary ecuations for each (forcing zero both ends)
  // %Delta(i) = (Y(i+1) - Y(i))/(X(i+1) - X(i))
  // %DeltaY(1)=Delta(i+1)-Delta(i)
  float DeltaY[N];
  for(int i=0;i<N;i++)
  {
      if(i==0) //%First
          DeltaY[i]=6*(((Y[i+1]-Y[i])/(X[i+1]-X[i]))-0);
      else
      {
          if(i==N-1) //%i=FINAL i
            DeltaY[i]=6*(0-((Y[i]-Y[i-1])/(X[i]-X[i-1])));
          else
            DeltaY[i]=6*(((Y[i+1]-Y[i])/(X[i+1]-X[i]))-((Y[i]-Y[i-1])/(X[i]-X[i-1])));
      }      
  }  
  // %We run the primary matrix using Gauss method - Arranged as a 1D array
  // %STARTING WITH i=1 ; pay atention to the array index
  for(int i=InitialIndex;i<=N-2+InitialIndex;i++)//%ROW
  {
      int DiagonalRow=i-InitialIndex;
      int DiagonalColumn=i;
      int DiagonalPosition=N*DiagonalRow+DiagonalColumn;
      int DiagonalLowerPosition=DiagonalPosition+N;
      float Factor=-PrimaryMatrix[DiagonalLowerPosition]/PrimaryMatrix[DiagonalPosition];   
      //%Gauss towards the diagonal
      if(i==InitialIndex)
      {
          PrimaryMatrix[DiagonalLowerPosition]=PrimaryMatrix[DiagonalLowerPosition]+Factor*PrimaryMatrix[DiagonalPosition];       
          PrimaryMatrix[DiagonalLowerPosition+1]=PrimaryMatrix[DiagonalLowerPosition+1]+Factor*PrimaryMatrix[DiagonalPosition+1];        
      }
      else
      {
          PrimaryMatrix[DiagonalLowerPosition-1]=PrimaryMatrix[DiagonalLowerPosition-1]+Factor*PrimaryMatrix[DiagonalPosition-1];        
          PrimaryMatrix[DiagonalLowerPosition]=PrimaryMatrix[DiagonalLowerPosition]+Factor*PrimaryMatrix[DiagonalPosition];
          PrimaryMatrix[DiagonalLowerPosition+1]=PrimaryMatrix[DiagonalLowerPosition+1]+Factor*PrimaryMatrix[DiagonalPosition+1];                
      }
  }
  /******************************ERROR****************************************************/
  float V[N];
  for(int i=N-1+InitialIndex;i>=InitialIndex;i=i-1)
  {
      int DiagonalRow=i-InitialIndex;
      int DiagonalColumn=i;    
      int DiagonalPosition=N*DiagonalRow+DiagonalColumn;
      V[i]=DeltaY[i];
      for(int j=i;j<=N-1+InitialIndex;j++)
  	{
          V[i]=V[i]-PrimaryMatrix[j];
  	}
      V[i]=V[i]/PrimaryMatrix[DiagonalPosition];       
  }
  
  for(int i=0;i<=N-2;i++)
  {
      /*a[i]*/Constant[i]=(V[i+1]-V[i])/(6*(X[i+1]-X[i]));
      /*b[i]*/Constant[i+N-1]=V[i]/2;
      /*c[i]*/Constant[i+2*(N-1)]=(Y[i+1]-Y[i])/(X[i+1]-X[i])-(2*V[i]+V[i+1])*(X[i+1]-X[i])/6;
      /*d[i]*/Constant[i+3*(N-1)]=Y[i];
      //t=X(i):0.1:X(i+1);
      //Spline=(a(i)*(t-X(i)).^3)+(b(i)*(t-X(i)).^2)+(c(i)*(t-X(i)))+d(i);
      // plot(t,Spline);hold on     	
  }
}

float TrajectoryPlanner::CubicSplineRunner(float * Constant, float Time)
{
	float Trajectory,a,b,c,d,cTime;

	cTime=this->TimeHandler(Time);	
	
	a=Constant[TrackNumber];
	b=Constant[TrackNumber+NumberOfSamples];
	c=Constant[TrackNumber+2*NumberOfSamples];
	d=Constant[TrackNumber+3*NumberOfSamples];	
	//Spline=(a(i)*(t-X(i)).^3)+(b(i)*(t-X(i)).^2)+(c(i)*(t-X(i)))+d(i);  
	Trajectory=a*cTime*cTime*cTime+b*cTime*cTime+c*cTime+d;
	
	return Trajectory;
}

int TrajectoryPlanner::TrackHandler(float Time)
{
	if(Time<FinalTime)
		TrackNumber=floor(Time/TrackTime);
	else
		TrackNumber=NumberOfSamples-1;
	return TrackNumber;
}

float TrajectoryPlanner::TimeHandler(float Time)
{
	float RunningTime;
	//This function take cares about InitialTime
	if(Running)
	{
		RunningTime=Time-InitialTime;
	}
	else
	{
		//Updates InitialTime
		InitialTime=Time;
		Running=Time-InitialTime;
		Running=True;	
	}
	
	float cTime;
	//Be sure that: cTime<<10*NumberOfSamples 
	this->TrackHandler(RunningTime);
	if(RunningTime<FinalTime)
	{
		cTime=(RunningTime/CubicTimeFactor-TrackNumber*TrackTime/CubicTimeFactor);		
	}
	else
	{
		cTime=TrackTime/CubicTimeFactor;	
	}
	
	if(RunningTime>FinalTime)
	{
		FinishedTrajectory=True;
	}
	return cTime;
}

void TrajectoryPlanner::Runner()
{
	Hip=this->CubicSplineRunner(&HipConstants[0],millis());
    Knee=this->CubicSplineRunner(&KneeConstants[0],millis());
}

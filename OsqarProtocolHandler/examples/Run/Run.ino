#include <OsqarProtocolHandler.h>
#include <OsqarSerialProtocol.h>
#include <QuadrupedGait.h>
#include <StateMachine.h>
#include <BaBauHound.h>
#include <LegMovementManager.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>
#include <DifferentialADC.h>

OsqarHandler Handler;

void setup()
{
  Handler.Initialize();
  Serial.begin(57600);
  while(Serial.available()<=0)
  {
  }
}

void loop()
{
  Handler.Runner();
}

#include <BaBauHound.h>

#define HipP 6
#define KneeP 10
#define AHipCorrect 0
#define AKneeCorrect 0
#define BHipCorrect 0
#define BKneeCorrect 0
#define CHipCorrect 0
#define CKneeCorrect 0
#define DHipCorrect 0
#define DKneeCorrect 0
#define Velocity 0.075 //Max 0.3 º/mseg
#define Rx 60
#define Height 20
#define MaxHeight 140

#define HighAngle 120
#define LowAngle  10
#define PWMPeriod 2500


Leg LegD,LegB,LegA,LegC;
LegMovementManager LegManager;
float ActualTime=0;
float LastTime=0;
int ActualUSecs=500;

byte i=0;

void setup()
{
  Serial.begin(57600);
  Setup();
  while(Serial.available()<=0)
  {
  }
  LegD.Create(HipP,KneeP,DHipCorrect,DKneeCorrect,LegManager);
  LegD.InvertHip=FALSE;
  LegD.InvertKnee=FALSE;
  LegD.MoveHip(500);
  delay(1000);  
}

void loop()
{
  ActualTime=millis();
  if(ActualTime-LastTime>PWMPeriod)
  {
    LastTime=millis();
//    ActualUSecs=ActualUSecs+20;    
//    LegD.MoveKnee(ActualUSecs);
    if(i%2==0)
      LegD.MoveKnee(HighAngle);
    else
      LegD.MoveKnee(LowAngle);
    i++;
  }
  SerialSample(LegD.ReadKnee());
}
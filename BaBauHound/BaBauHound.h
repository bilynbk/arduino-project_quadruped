/*##################################
**			BaBauHound			  **
##################################*/

/*	UpSide View
		
		Head
	[0]______[1]
	   
	[2]______[3]
	
			
			
			Width
			
			Head
		____________
		|			|
Large	|			|
		|			|
		|			|
		|___________|
	
	
	LSPB Trajectory
	
        Velocity
             |
MaxVelocity  |   __________
             |  /          \
             | /            \
            0|/______________\____time 
             to tb      tf-tb tf	

Parallel Movement V2 - SampleTime depends on the Change of position (every 1º it samples)
*/
/*
  BaBauHound
  
	v0.1 to 1.0
    New class Quadruped with own methods. Creates 4 class type Leg, each one with its own Hip and Knee Corrections.
      -Could be calibrated manually or by Leg::Homming (needs to be fixed) method
    
    Leg Class
	  -New Method RectangularMovement
	  
	v2.1
	-New TrajectoryPlanner for every Leg. Can be used with LegMovementManager for movement enque.
	-New Gait Planner with its own StateMachine.
	-New Quadruped CLASS with COG referenced to leg 3
*/


//import from sketch
#ifndef BaBauHound_h
#define BaBauHound_h


#include "Servo.h"
//#include "DifferentialADC.h"
//#include "LabViewProtocol.h"

#include <DifferentialADC.h>
#include <Arduino.h>
#include <math.h>
#include <inttypes.h>
#include <Queu.h>
#include <TrajectoryPlanner.h>

class Leg;
class Quadruped;

#define TRUE 1
#define FALSE 0
#define MaxPulseTime 2600 //Microseconds
#define MinPulseTime 500 //Microseconds
#define HipLink 75
#define KneeLink 90
#define AnkleLink 40
#define ElbowDown 0
#define ElbowUp 1
#define Resolution 0.3  //Such that max servo movement 0.3º/mseg
#define MinHip 0
#define MaxHip 180
#define MinKnee 0
#define MaxKnee 180

class Leg
{
	public:
		//Constructor & Setup
		void Create(int, int);
		void Create(int, int, int, int);
        void Create(int,int,int,int,bool);
        void Homming(int);
		
		void SetJointOffset(int,int);
		
		//Destructor
        bool Delete();
		
		//Status
        bool IsActive();
        float ReadHip();
        float ReadKnee();
        float ReadPositionX();
		float ReadPositionY();
        float ReadHipMicroseconds();
		float ReadKneeMicroseconds();		

		//Simple Move
        bool MoveHip(float);
        bool MoveKnee(float);
		                
		//Movement
		void NewMovement(float,float,float,float,int);	//Setup
		bool Move();
		
		//MovementStatus
		bool FinishedMovement();
		bool InvertHip;
		bool InvertKnee;
		bool ElbowUpSelect;
		
		//Methods that OsqarProtocol Uses
		void UpdateHipByteValue();
		void UpdateKneeByteValue();
		uint8_t * ReadHipHAddress();
		uint8_t * ReadHipLAddress();
		uint8_t * ReadKneeHAddress();
		uint8_t * ReadKneeLAddress();
		
		byte ID;
		TrajectoryPlanner Traject;
	private:
		float Hip;//Should be private
		float Knee;//Should be private		
        float HipCorrection;
		float KneeCorrection;
		Servo ServoHip;
		Servo ServoKnee;
		
		//stuff that OsqarProtocolUses
		uint8_t UnsignedHipH;
		uint8_t UnsignedHipL;		
		uint8_t UnsignedKneeH;
		uint8_t UnsignedKneeL;				

};

//
//		Quadruped CLASS
//

#define FHipsDistance 188 //mm
#define RHipsDistances 166 //mm
#define FrontToRearHipDistance 195 //mm

#define HipP 6
#define KneeP 10

#define Leg1HipCorrection 0
#define Leg2HipCorrection 0
#define Leg3HipCorrection 0
#define Leg4HipCorrection 0

#define Leg1KneeCorrection 0
#define Leg2KneeCorrection 0
#define Leg3KneeCorrection 0
#define Leg4KneeCorrection 0

 class Quadruped
 {
	 public:
		 void Initialize(/*,QuadrupedGait*/);
		 Leg Legs[4];
		// QuadrupedGait * ImplementedGait;
		
		 int Length;
		 int FrontWidth;
		 int RearWidth;	
		 int COGX;
		 int COGY;
		 int COGZ;
		
		 void RefreshCOGCoordinates();
 };

#endif


